package Assigments;

public class Calculator {
    public interface Calculate {
        abstract int calculate(int operand1, int operand2, Operator operator);
    }

    public enum Operator {
        ADD, SUBTRACT, DIVIDE, MULTIPLY;
    }

    public static class CalculateImpl implements Calculate {

        @Override
        public int calculate(int operand1, int operand2, Operator operator) {

            switch (operator) {
                case ADD:
                    return operand1 + operand2;
                case SUBTRACT:
                    return operand1 - operand2;
                case MULTIPLY:
                    return operand1 * operand2;
                case DIVIDE:
                    return operand1 / operand2;
            }

            throw new RuntimeException(operator + "is unsupported");
        }

    }

    public static class CalculatorTest {
        public static void main(String[] args) {
            Calculate calc = new CalculateImpl();
            int result = calc.calculate(5,6,Operator.ADD);
            result = calc.calculate(result,6,Operator.MULTIPLY);
            result = calc.calculate(result,1,Operator.SUBTRACT);
            result = calc.calculate(result,5,Operator.DIVIDE);

            System.out.println("result=" + result);
        }
    }
}
